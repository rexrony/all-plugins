��          �   %   �      p  
   q     |     �     �     �     �     �     �     �            a        |     �     �     �     �     �                    #  !   5     W     ]     w  %   �  �  �     `     s     {     �     �     �     �     �     �  
   �               1     F     [     v     �     �     �     �     �     �  "   �     �  
   �  
   	     	                                                                             	                   
                           Add / Edit Add New Add New Field Group Add new address Advanced Custom Fields All Close Window Default billing address Default shipping address Field Group Field Groups Identifier / Name (it is used to identify this address for a future usage. Ex.: "Office address") Last used billing address Last used shipping address Latest Used Addresses Latest Version Move to trash. Are you sure? Placeholder Text Register Save Select a country Select an address There are no additional addresses Title VAT Identification Number VAT Identification Number: placeholderVAT Identification Number Project-Id-Version: WooCommerce Multiple Customer Addresses
POT-Creation-Date: 2017-02-08 12:08+0100
PO-Revision-Date: 2017-02-08 12:08+0100
Last-Translator: Marijke <marijke@mergenmetz.nl>
Language-Team: 
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.7.5
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Toevoegen/Bewerken Add New Nieuw Veld Groep Toevoegen Voeg nieuw adres toe Advanced Custom Fields Alle Sluit venster Factuuradres Standaard verzendadres Veld Groep Veld Groepen Naam (om adres terug te vinden) Laatste factuuradres Laatste verzendadres Laatst gebruikte addressen Laatste versie Verwijder. Zeker weten? vul in Registreren Opslaan Land Selecteer een adres Er zijn geen toegevoegde addressen Titel BTW-nummer Btw-nummer BTW-nummer  